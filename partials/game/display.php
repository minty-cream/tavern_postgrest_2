<div class="card is-fullwidth">
  <header class="card-header">
    <p class="card-header-title">
	{{game.title}}
    </p>
  </header>
  <div class="card-content">
    <div class="content">
Really nice description
    </div>
  </div>
  <footer class="card-footer">
    <span class="card-footer-item"><strong>Rules:</strong> {{game.rulebook}}</span>
    <span class="card-footer-item"><strong>GM:</strong> {{game.game_master}}</span>
    <span class="card-footer-item"><strong>First Game:</strong> {{game.first_game}}</span>
  </footer>
</div>
