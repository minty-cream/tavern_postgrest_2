<!doctype html>
<html lang="en" ng-app="tavern">
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.17/css/bulma.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/restangular/1.3.1/restangular.js"></script>
<script type="text/javascript" src="tavern.js"></script>
</head>
<body>
<header class="header is-primary">
<div class="container">
	<div class="header-left">
		<a class="header-item" href="#">Tavern</a>
	</div>
	<div class="header-right header-menu">
<?php foreach(array('toon','game','player') as $obj){ ?>
		<a 
		class="header-tab <?php print $_GET['obj']===$obj?"is-active":"";?>"
		href="/?obj=<?php print $obj; ?>">
			<?php print $obj;?>
		</a>
<?php } ?>
	</div>
</div>
</header>
<section class="section">
	<div class="container <?php print($_GET['obj']);?>Ctrl" ng-controller="<?php print($_GET['obj']);?>Ctrl">
			<h1>Create <?php print($_GET['obj']);?></h1>
			<form ng-submit="create_<?php print($_GET['obj']);?>()">
				<?php require __DIR__."/partials/".$_GET['obj']."/create.php" ?>
				<input type="submit">
			</form>
			<h2><?php print($_GET['obj']);?>s</h2>
			<div class="columns" ng-repeat="<?php print($_GET['obj']);?> in <?php print($_GET['obj']);?>s">
				<div class="column">
					<?php require __DIR__."/partials/".$_GET['obj']."/display.php" ?>
				</div>
			</div>
	</div>
</section>
</body>
</html>
