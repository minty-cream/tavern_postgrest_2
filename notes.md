Tech stack
==========

Backend
:	[PostGrest](http://postgrest.com/examples/start/)

File Server
:	[spas](https://github.com/srid/spas)

Auth
:	See Backend

Frontend
:	[MiddleMan](https://middlemanapp.com/)


